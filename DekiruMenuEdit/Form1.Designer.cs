﻿namespace DekiruMenuEdit
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tbWhole = new System.Windows.Forms.RichTextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.lblCount4 = new System.Windows.Forms.Label();
            this.tbString4 = new System.Windows.Forms.TextBox();
            this.lblBlock = new System.Windows.Forms.Label();
            this.lblCount3 = new System.Windows.Forms.Label();
            this.lblCount2 = new System.Windows.Forms.Label();
            this.lblCount1 = new System.Windows.Forms.Label();
            this.tbString3 = new System.Windows.Forms.TextBox();
            this.tbString2 = new System.Windows.Forms.TextBox();
            this.tbString1 = new System.Windows.Forms.TextBox();
            this.lblBoy11 = new System.Windows.Forms.Label();
            this.txtBoy11 = new System.Windows.Forms.TextBox();
            this.lblGirl11 = new System.Windows.Forms.Label();
            this.txtGirl11 = new System.Windows.Forms.TextBox();
            this.lblBoy10 = new System.Windows.Forms.Label();
            this.txtBoy10 = new System.Windows.Forms.TextBox();
            this.lblBoy9 = new System.Windows.Forms.Label();
            this.txtBoy9 = new System.Windows.Forms.TextBox();
            this.lblBoy8 = new System.Windows.Forms.Label();
            this.txtBoy8 = new System.Windows.Forms.TextBox();
            this.lblBoy7 = new System.Windows.Forms.Label();
            this.lblBoy6 = new System.Windows.Forms.Label();
            this.lblBoy5 = new System.Windows.Forms.Label();
            this.txtBoy7 = new System.Windows.Forms.TextBox();
            this.txtBoy6 = new System.Windows.Forms.TextBox();
            this.txtBoy5 = new System.Windows.Forms.TextBox();
            this.lblBoy4 = new System.Windows.Forms.Label();
            this.txtBoy4 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.lblBoy3 = new System.Windows.Forms.Label();
            this.lblBoy2 = new System.Windows.Forms.Label();
            this.lblBoy1 = new System.Windows.Forms.Label();
            this.txtBoy3 = new System.Windows.Forms.TextBox();
            this.txtBoy2 = new System.Windows.Forms.TextBox();
            this.txtBoy1 = new System.Windows.Forms.TextBox();
            this.lblGirl10 = new System.Windows.Forms.Label();
            this.txtGirl10 = new System.Windows.Forms.TextBox();
            this.lblGirl9 = new System.Windows.Forms.Label();
            this.txtGirl9 = new System.Windows.Forms.TextBox();
            this.lblGirl8 = new System.Windows.Forms.Label();
            this.txtGirl8 = new System.Windows.Forms.TextBox();
            this.lblGirl7 = new System.Windows.Forms.Label();
            this.lblGirl6 = new System.Windows.Forms.Label();
            this.lblGirl5 = new System.Windows.Forms.Label();
            this.txtGirl7 = new System.Windows.Forms.TextBox();
            this.txtGirl6 = new System.Windows.Forms.TextBox();
            this.txtGirl5 = new System.Windows.Forms.TextBox();
            this.lblGirl4 = new System.Windows.Forms.Label();
            this.txtGirl4 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblGirl3 = new System.Windows.Forms.Label();
            this.lblGirl2 = new System.Windows.Forms.Label();
            this.lblGirl1 = new System.Windows.Forms.Label();
            this.txtGirl3 = new System.Windows.Forms.TextBox();
            this.txtGirl2 = new System.Windows.Forms.TextBox();
            this.txtGirl1 = new System.Windows.Forms.TextBox();
            this.label2934 = new System.Windows.Forms.Label();
            this.richBoy = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.richGirl = new System.Windows.Forms.RichTextBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1172, 28);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(152, 24);
            this.openToolStripMenuItem.Text = "&Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(152, 24);
            this.saveToolStripMenuItem.Text = "&Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(152, 24);
            this.saveAsToolStripMenuItem.Text = "S&ave As";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 24);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 28);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tbWhole);
            this.splitContainer1.Panel1.Controls.Add(this.label25);
            this.splitContainer1.Panel1.Controls.Add(this.treeView1);
            this.splitContainer1.Panel1.Controls.Add(this.lblCount4);
            this.splitContainer1.Panel1.Controls.Add(this.tbString4);
            this.splitContainer1.Panel1.Controls.Add(this.lblBlock);
            this.splitContainer1.Panel1.Controls.Add(this.lblCount3);
            this.splitContainer1.Panel1.Controls.Add(this.lblCount2);
            this.splitContainer1.Panel1.Controls.Add(this.lblCount1);
            this.splitContainer1.Panel1.Controls.Add(this.tbString3);
            this.splitContainer1.Panel1.Controls.Add(this.tbString2);
            this.splitContainer1.Panel1.Controls.Add(this.tbString1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.lblBoy11);
            this.splitContainer1.Panel2.Controls.Add(this.txtBoy11);
            this.splitContainer1.Panel2.Controls.Add(this.lblGirl11);
            this.splitContainer1.Panel2.Controls.Add(this.txtGirl11);
            this.splitContainer1.Panel2.Controls.Add(this.lblBoy10);
            this.splitContainer1.Panel2.Controls.Add(this.txtBoy10);
            this.splitContainer1.Panel2.Controls.Add(this.lblBoy9);
            this.splitContainer1.Panel2.Controls.Add(this.txtBoy9);
            this.splitContainer1.Panel2.Controls.Add(this.lblBoy8);
            this.splitContainer1.Panel2.Controls.Add(this.txtBoy8);
            this.splitContainer1.Panel2.Controls.Add(this.lblBoy7);
            this.splitContainer1.Panel2.Controls.Add(this.lblBoy6);
            this.splitContainer1.Panel2.Controls.Add(this.lblBoy5);
            this.splitContainer1.Panel2.Controls.Add(this.txtBoy7);
            this.splitContainer1.Panel2.Controls.Add(this.txtBoy6);
            this.splitContainer1.Panel2.Controls.Add(this.txtBoy5);
            this.splitContainer1.Panel2.Controls.Add(this.lblBoy4);
            this.splitContainer1.Panel2.Controls.Add(this.txtBoy4);
            this.splitContainer1.Panel2.Controls.Add(this.label21);
            this.splitContainer1.Panel2.Controls.Add(this.lblBoy3);
            this.splitContainer1.Panel2.Controls.Add(this.lblBoy2);
            this.splitContainer1.Panel2.Controls.Add(this.lblBoy1);
            this.splitContainer1.Panel2.Controls.Add(this.txtBoy3);
            this.splitContainer1.Panel2.Controls.Add(this.txtBoy2);
            this.splitContainer1.Panel2.Controls.Add(this.txtBoy1);
            this.splitContainer1.Panel2.Controls.Add(this.lblGirl10);
            this.splitContainer1.Panel2.Controls.Add(this.txtGirl10);
            this.splitContainer1.Panel2.Controls.Add(this.lblGirl9);
            this.splitContainer1.Panel2.Controls.Add(this.txtGirl9);
            this.splitContainer1.Panel2.Controls.Add(this.lblGirl8);
            this.splitContainer1.Panel2.Controls.Add(this.txtGirl8);
            this.splitContainer1.Panel2.Controls.Add(this.lblGirl7);
            this.splitContainer1.Panel2.Controls.Add(this.lblGirl6);
            this.splitContainer1.Panel2.Controls.Add(this.lblGirl5);
            this.splitContainer1.Panel2.Controls.Add(this.txtGirl7);
            this.splitContainer1.Panel2.Controls.Add(this.txtGirl6);
            this.splitContainer1.Panel2.Controls.Add(this.txtGirl5);
            this.splitContainer1.Panel2.Controls.Add(this.lblGirl4);
            this.splitContainer1.Panel2.Controls.Add(this.txtGirl4);
            this.splitContainer1.Panel2.Controls.Add(this.label4);
            this.splitContainer1.Panel2.Controls.Add(this.lblGirl3);
            this.splitContainer1.Panel2.Controls.Add(this.lblGirl2);
            this.splitContainer1.Panel2.Controls.Add(this.lblGirl1);
            this.splitContainer1.Panel2.Controls.Add(this.txtGirl3);
            this.splitContainer1.Panel2.Controls.Add(this.txtGirl2);
            this.splitContainer1.Panel2.Controls.Add(this.txtGirl1);
            this.splitContainer1.Panel2.Controls.Add(this.label2934);
            this.splitContainer1.Panel2.Controls.Add(this.richBoy);
            this.splitContainer1.Panel2.Controls.Add(this.label2);
            this.splitContainer1.Panel2.Controls.Add(this.richGirl);
            this.splitContainer1.Size = new System.Drawing.Size(1172, 655);
            this.splitContainer1.SplitterDistance = 503;
            this.splitContainer1.TabIndex = 3;
            // 
            // tbWhole
            // 
            this.tbWhole.Font = new System.Drawing.Font("Meiryo", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbWhole.Location = new System.Drawing.Point(168, 183);
            this.tbWhole.Name = "tbWhole";
            this.tbWhole.ReadOnly = true;
            this.tbWhole.Size = new System.Drawing.Size(271, 100);
            this.tbWhole.TabIndex = 121;
            this.tbWhole.Text = "";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(165, 163);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(93, 17);
            this.label25.TabIndex = 129;
            this.label25.Text = "Whole String:";
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(12, 8);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(147, 583);
            this.treeView1.TabIndex = 116;
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            // 
            // lblCount4
            // 
            this.lblCount4.AutoSize = true;
            this.lblCount4.Location = new System.Drawing.Point(445, 135);
            this.lblCount4.Name = "lblCount4";
            this.lblCount4.Size = new System.Drawing.Size(28, 17);
            this.lblCount4.TabIndex = 127;
            this.lblCount4.Text = "0/0";
            // 
            // tbString4
            // 
            this.tbString4.Enabled = false;
            this.tbString4.Font = new System.Drawing.Font("Meiryo", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbString4.ImeMode = System.Windows.Forms.ImeMode.AlphaFull;
            this.tbString4.Location = new System.Drawing.Point(168, 127);
            this.tbString4.MaxLength = 8;
            this.tbString4.Name = "tbString4";
            this.tbString4.Size = new System.Drawing.Size(271, 33);
            this.tbString4.TabIndex = 120;
            this.tbString4.TextChanged += new System.EventHandler(this.tbString4_TextChanged_1);
            // 
            // lblBlock
            // 
            this.lblBlock.AutoSize = true;
            this.lblBlock.Location = new System.Drawing.Point(165, 8);
            this.lblBlock.Name = "lblBlock";
            this.lblBlock.Size = new System.Drawing.Size(69, 17);
            this.lblBlock.TabIndex = 125;
            this.lblBlock.Text = "Message:";
            // 
            // lblCount3
            // 
            this.lblCount3.AutoSize = true;
            this.lblCount3.Location = new System.Drawing.Point(445, 102);
            this.lblCount3.Name = "lblCount3";
            this.lblCount3.Size = new System.Drawing.Size(28, 17);
            this.lblCount3.TabIndex = 124;
            this.lblCount3.Text = "0/0";
            // 
            // lblCount2
            // 
            this.lblCount2.AutoSize = true;
            this.lblCount2.Location = new System.Drawing.Point(445, 69);
            this.lblCount2.Name = "lblCount2";
            this.lblCount2.Size = new System.Drawing.Size(28, 17);
            this.lblCount2.TabIndex = 122;
            this.lblCount2.Text = "0/0";
            // 
            // lblCount1
            // 
            this.lblCount1.AutoSize = true;
            this.lblCount1.Location = new System.Drawing.Point(445, 39);
            this.lblCount1.Name = "lblCount1";
            this.lblCount1.Size = new System.Drawing.Size(28, 17);
            this.lblCount1.TabIndex = 120;
            this.lblCount1.Text = "0/0";
            // 
            // tbString3
            // 
            this.tbString3.Enabled = false;
            this.tbString3.Font = new System.Drawing.Font("Meiryo", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbString3.ImeMode = System.Windows.Forms.ImeMode.AlphaFull;
            this.tbString3.Location = new System.Drawing.Point(168, 94);
            this.tbString3.MaxLength = 8;
            this.tbString3.Name = "tbString3";
            this.tbString3.Size = new System.Drawing.Size(271, 33);
            this.tbString3.TabIndex = 119;
            this.tbString3.TextChanged += new System.EventHandler(this.tbString3_TextChanged_1);
            // 
            // tbString2
            // 
            this.tbString2.Enabled = false;
            this.tbString2.Font = new System.Drawing.Font("Meiryo", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbString2.ImeMode = System.Windows.Forms.ImeMode.AlphaFull;
            this.tbString2.Location = new System.Drawing.Point(168, 61);
            this.tbString2.MaxLength = 8;
            this.tbString2.Name = "tbString2";
            this.tbString2.Size = new System.Drawing.Size(271, 33);
            this.tbString2.TabIndex = 118;
            this.tbString2.TextChanged += new System.EventHandler(this.tbString2_TextChanged_1);
            // 
            // tbString1
            // 
            this.tbString1.Enabled = false;
            this.tbString1.Font = new System.Drawing.Font("Meiryo", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbString1.ImeMode = System.Windows.Forms.ImeMode.AlphaFull;
            this.tbString1.Location = new System.Drawing.Point(168, 28);
            this.tbString1.MaxLength = 8;
            this.tbString1.Name = "tbString1";
            this.tbString1.Size = new System.Drawing.Size(271, 33);
            this.tbString1.TabIndex = 117;
            this.tbString1.TextChanged += new System.EventHandler(this.tbString1_TextChanged);
            // 
            // lblBoy11
            // 
            this.lblBoy11.AutoSize = true;
            this.lblBoy11.Location = new System.Drawing.Point(609, 365);
            this.lblBoy11.Name = "lblBoy11";
            this.lblBoy11.Size = new System.Drawing.Size(36, 17);
            this.lblBoy11.TabIndex = 223;
            this.lblBoy11.Text = "0/10";
            // 
            // txtBoy11
            // 
            this.txtBoy11.Enabled = false;
            this.txtBoy11.Font = new System.Drawing.Font("Meiryo", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoy11.ImeMode = System.Windows.Forms.ImeMode.AlphaFull;
            this.txtBoy11.Location = new System.Drawing.Point(332, 360);
            this.txtBoy11.MaxLength = 10;
            this.txtBoy11.Name = "txtBoy11";
            this.txtBoy11.Size = new System.Drawing.Size(271, 33);
            this.txtBoy11.TabIndex = 144;
            this.txtBoy11.TextChanged += new System.EventHandler(this.txtBoy11_TextChanged);
            // 
            // lblGirl11
            // 
            this.lblGirl11.AutoSize = true;
            this.lblGirl11.Location = new System.Drawing.Point(280, 368);
            this.lblGirl11.Name = "lblGirl11";
            this.lblGirl11.Size = new System.Drawing.Size(36, 17);
            this.lblGirl11.TabIndex = 221;
            this.lblGirl11.Text = "0/10";
            // 
            // txtGirl11
            // 
            this.txtGirl11.Enabled = false;
            this.txtGirl11.Font = new System.Drawing.Font("Meiryo", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGirl11.ImeMode = System.Windows.Forms.ImeMode.AlphaFull;
            this.txtGirl11.Location = new System.Drawing.Point(3, 360);
            this.txtGirl11.MaxLength = 10;
            this.txtGirl11.Name = "txtGirl11";
            this.txtGirl11.Size = new System.Drawing.Size(271, 33);
            this.txtGirl11.TabIndex = 132;
            this.txtGirl11.TextChanged += new System.EventHandler(this.txtGirl11_TextChanged);
            // 
            // lblBoy10
            // 
            this.lblBoy10.AutoSize = true;
            this.lblBoy10.Location = new System.Drawing.Point(609, 332);
            this.lblBoy10.Name = "lblBoy10";
            this.lblBoy10.Size = new System.Drawing.Size(36, 17);
            this.lblBoy10.TabIndex = 219;
            this.lblBoy10.Text = "0/10";
            // 
            // txtBoy10
            // 
            this.txtBoy10.Enabled = false;
            this.txtBoy10.Font = new System.Drawing.Font("Meiryo", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoy10.ImeMode = System.Windows.Forms.ImeMode.AlphaFull;
            this.txtBoy10.Location = new System.Drawing.Point(332, 327);
            this.txtBoy10.MaxLength = 10;
            this.txtBoy10.Name = "txtBoy10";
            this.txtBoy10.Size = new System.Drawing.Size(271, 33);
            this.txtBoy10.TabIndex = 143;
            this.txtBoy10.TextChanged += new System.EventHandler(this.txtBoy10_TextChanged);
            // 
            // lblBoy9
            // 
            this.lblBoy9.AutoSize = true;
            this.lblBoy9.Location = new System.Drawing.Point(609, 299);
            this.lblBoy9.Name = "lblBoy9";
            this.lblBoy9.Size = new System.Drawing.Size(36, 17);
            this.lblBoy9.TabIndex = 218;
            this.lblBoy9.Text = "0/10";
            // 
            // txtBoy9
            // 
            this.txtBoy9.Enabled = false;
            this.txtBoy9.Font = new System.Drawing.Font("Meiryo", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoy9.ImeMode = System.Windows.Forms.ImeMode.AlphaFull;
            this.txtBoy9.Location = new System.Drawing.Point(332, 294);
            this.txtBoy9.MaxLength = 10;
            this.txtBoy9.Name = "txtBoy9";
            this.txtBoy9.Size = new System.Drawing.Size(271, 33);
            this.txtBoy9.TabIndex = 142;
            this.txtBoy9.TextChanged += new System.EventHandler(this.txtBoy9_TextChanged);
            // 
            // lblBoy8
            // 
            this.lblBoy8.AutoSize = true;
            this.lblBoy8.Location = new System.Drawing.Point(609, 266);
            this.lblBoy8.Name = "lblBoy8";
            this.lblBoy8.Size = new System.Drawing.Size(36, 17);
            this.lblBoy8.TabIndex = 215;
            this.lblBoy8.Text = "0/10";
            // 
            // txtBoy8
            // 
            this.txtBoy8.Enabled = false;
            this.txtBoy8.Font = new System.Drawing.Font("Meiryo", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoy8.ImeMode = System.Windows.Forms.ImeMode.AlphaFull;
            this.txtBoy8.Location = new System.Drawing.Point(332, 261);
            this.txtBoy8.MaxLength = 10;
            this.txtBoy8.Name = "txtBoy8";
            this.txtBoy8.Size = new System.Drawing.Size(271, 33);
            this.txtBoy8.TabIndex = 141;
            this.txtBoy8.TextChanged += new System.EventHandler(this.txtBoy8_TextChanged);
            // 
            // lblBoy7
            // 
            this.lblBoy7.AutoSize = true;
            this.lblBoy7.Location = new System.Drawing.Point(609, 233);
            this.lblBoy7.Name = "lblBoy7";
            this.lblBoy7.Size = new System.Drawing.Size(36, 17);
            this.lblBoy7.TabIndex = 214;
            this.lblBoy7.Text = "0/10";
            // 
            // lblBoy6
            // 
            this.lblBoy6.AutoSize = true;
            this.lblBoy6.Location = new System.Drawing.Point(609, 200);
            this.lblBoy6.Name = "lblBoy6";
            this.lblBoy6.Size = new System.Drawing.Size(36, 17);
            this.lblBoy6.TabIndex = 213;
            this.lblBoy6.Text = "0/10";
            // 
            // lblBoy5
            // 
            this.lblBoy5.AutoSize = true;
            this.lblBoy5.Location = new System.Drawing.Point(609, 167);
            this.lblBoy5.Name = "lblBoy5";
            this.lblBoy5.Size = new System.Drawing.Size(36, 17);
            this.lblBoy5.TabIndex = 212;
            this.lblBoy5.Text = "0/10";
            // 
            // txtBoy7
            // 
            this.txtBoy7.Enabled = false;
            this.txtBoy7.Font = new System.Drawing.Font("Meiryo", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoy7.ImeMode = System.Windows.Forms.ImeMode.AlphaFull;
            this.txtBoy7.Location = new System.Drawing.Point(332, 228);
            this.txtBoy7.MaxLength = 10;
            this.txtBoy7.Name = "txtBoy7";
            this.txtBoy7.Size = new System.Drawing.Size(271, 33);
            this.txtBoy7.TabIndex = 140;
            this.txtBoy7.TextChanged += new System.EventHandler(this.txtBoy7_TextChanged);
            // 
            // txtBoy6
            // 
            this.txtBoy6.Enabled = false;
            this.txtBoy6.Font = new System.Drawing.Font("Meiryo", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoy6.ImeMode = System.Windows.Forms.ImeMode.AlphaFull;
            this.txtBoy6.Location = new System.Drawing.Point(332, 195);
            this.txtBoy6.MaxLength = 10;
            this.txtBoy6.Name = "txtBoy6";
            this.txtBoy6.Size = new System.Drawing.Size(271, 33);
            this.txtBoy6.TabIndex = 139;
            this.txtBoy6.TextChanged += new System.EventHandler(this.txtBoy6_TextChanged);
            // 
            // txtBoy5
            // 
            this.txtBoy5.Enabled = false;
            this.txtBoy5.Font = new System.Drawing.Font("Meiryo", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoy5.ImeMode = System.Windows.Forms.ImeMode.AlphaFull;
            this.txtBoy5.Location = new System.Drawing.Point(332, 162);
            this.txtBoy5.MaxLength = 10;
            this.txtBoy5.Name = "txtBoy5";
            this.txtBoy5.Size = new System.Drawing.Size(271, 33);
            this.txtBoy5.TabIndex = 138;
            this.txtBoy5.TextChanged += new System.EventHandler(this.txtBoy5_TextChanged);
            // 
            // lblBoy4
            // 
            this.lblBoy4.AutoSize = true;
            this.lblBoy4.Location = new System.Drawing.Point(609, 134);
            this.lblBoy4.Name = "lblBoy4";
            this.lblBoy4.Size = new System.Drawing.Size(36, 17);
            this.lblBoy4.TabIndex = 207;
            this.lblBoy4.Text = "0/10";
            // 
            // txtBoy4
            // 
            this.txtBoy4.Enabled = false;
            this.txtBoy4.Font = new System.Drawing.Font("Meiryo", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoy4.ImeMode = System.Windows.Forms.ImeMode.AlphaFull;
            this.txtBoy4.Location = new System.Drawing.Point(332, 129);
            this.txtBoy4.MaxLength = 10;
            this.txtBoy4.Name = "txtBoy4";
            this.txtBoy4.Size = new System.Drawing.Size(271, 33);
            this.txtBoy4.TabIndex = 137;
            this.txtBoy4.TextChanged += new System.EventHandler(this.txtBoy4_TextChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(332, 8);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(69, 17);
            this.label21.TabIndex = 206;
            this.label21.Text = "Message:";
            // 
            // lblBoy3
            // 
            this.lblBoy3.AutoSize = true;
            this.lblBoy3.Location = new System.Drawing.Point(609, 101);
            this.lblBoy3.Name = "lblBoy3";
            this.lblBoy3.Size = new System.Drawing.Size(36, 17);
            this.lblBoy3.TabIndex = 205;
            this.lblBoy3.Text = "0/10";
            // 
            // lblBoy2
            // 
            this.lblBoy2.AutoSize = true;
            this.lblBoy2.Location = new System.Drawing.Point(609, 68);
            this.lblBoy2.Name = "lblBoy2";
            this.lblBoy2.Size = new System.Drawing.Size(36, 17);
            this.lblBoy2.TabIndex = 204;
            this.lblBoy2.Text = "0/10";
            // 
            // lblBoy1
            // 
            this.lblBoy1.AutoSize = true;
            this.lblBoy1.Location = new System.Drawing.Point(609, 35);
            this.lblBoy1.Name = "lblBoy1";
            this.lblBoy1.Size = new System.Drawing.Size(36, 17);
            this.lblBoy1.TabIndex = 203;
            this.lblBoy1.Text = "0/10";
            // 
            // txtBoy3
            // 
            this.txtBoy3.Enabled = false;
            this.txtBoy3.Font = new System.Drawing.Font("Meiryo", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoy3.ImeMode = System.Windows.Forms.ImeMode.AlphaFull;
            this.txtBoy3.Location = new System.Drawing.Point(332, 96);
            this.txtBoy3.MaxLength = 10;
            this.txtBoy3.Name = "txtBoy3";
            this.txtBoy3.Size = new System.Drawing.Size(271, 33);
            this.txtBoy3.TabIndex = 136;
            this.txtBoy3.TextChanged += new System.EventHandler(this.txtBoy3_TextChanged);
            // 
            // txtBoy2
            // 
            this.txtBoy2.Enabled = false;
            this.txtBoy2.Font = new System.Drawing.Font("Meiryo", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoy2.ImeMode = System.Windows.Forms.ImeMode.AlphaFull;
            this.txtBoy2.Location = new System.Drawing.Point(332, 63);
            this.txtBoy2.MaxLength = 10;
            this.txtBoy2.Name = "txtBoy2";
            this.txtBoy2.Size = new System.Drawing.Size(271, 33);
            this.txtBoy2.TabIndex = 135;
            this.txtBoy2.TextChanged += new System.EventHandler(this.txtBoy2_TextChanged);
            // 
            // txtBoy1
            // 
            this.txtBoy1.Enabled = false;
            this.txtBoy1.Font = new System.Drawing.Font("Meiryo", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoy1.ImeMode = System.Windows.Forms.ImeMode.AlphaFull;
            this.txtBoy1.Location = new System.Drawing.Point(332, 30);
            this.txtBoy1.MaxLength = 10;
            this.txtBoy1.Name = "txtBoy1";
            this.txtBoy1.Size = new System.Drawing.Size(271, 33);
            this.txtBoy1.TabIndex = 134;
            this.txtBoy1.TextChanged += new System.EventHandler(this.txtBoy1_TextChanged);
            // 
            // lblGirl10
            // 
            this.lblGirl10.AutoSize = true;
            this.lblGirl10.Location = new System.Drawing.Point(280, 335);
            this.lblGirl10.Name = "lblGirl10";
            this.lblGirl10.Size = new System.Drawing.Size(36, 17);
            this.lblGirl10.TabIndex = 198;
            this.lblGirl10.Text = "0/10";
            // 
            // txtGirl10
            // 
            this.txtGirl10.Enabled = false;
            this.txtGirl10.Font = new System.Drawing.Font("Meiryo", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGirl10.ImeMode = System.Windows.Forms.ImeMode.AlphaFull;
            this.txtGirl10.Location = new System.Drawing.Point(3, 327);
            this.txtGirl10.MaxLength = 10;
            this.txtGirl10.Name = "txtGirl10";
            this.txtGirl10.Size = new System.Drawing.Size(271, 33);
            this.txtGirl10.TabIndex = 131;
            this.txtGirl10.TextChanged += new System.EventHandler(this.txtGirl10_TextChanged);
            // 
            // lblGirl9
            // 
            this.lblGirl9.AutoSize = true;
            this.lblGirl9.Location = new System.Drawing.Point(280, 302);
            this.lblGirl9.Name = "lblGirl9";
            this.lblGirl9.Size = new System.Drawing.Size(36, 17);
            this.lblGirl9.TabIndex = 197;
            this.lblGirl9.Text = "0/10";
            // 
            // txtGirl9
            // 
            this.txtGirl9.Enabled = false;
            this.txtGirl9.Font = new System.Drawing.Font("Meiryo", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGirl9.ImeMode = System.Windows.Forms.ImeMode.AlphaFull;
            this.txtGirl9.Location = new System.Drawing.Point(3, 294);
            this.txtGirl9.MaxLength = 10;
            this.txtGirl9.Name = "txtGirl9";
            this.txtGirl9.Size = new System.Drawing.Size(271, 33);
            this.txtGirl9.TabIndex = 130;
            this.txtGirl9.TextChanged += new System.EventHandler(this.txtGirl9_TextChanged);
            // 
            // lblGirl8
            // 
            this.lblGirl8.AutoSize = true;
            this.lblGirl8.Location = new System.Drawing.Point(280, 269);
            this.lblGirl8.Name = "lblGirl8";
            this.lblGirl8.Size = new System.Drawing.Size(36, 17);
            this.lblGirl8.TabIndex = 194;
            this.lblGirl8.Text = "0/10";
            // 
            // txtGirl8
            // 
            this.txtGirl8.Enabled = false;
            this.txtGirl8.Font = new System.Drawing.Font("Meiryo", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGirl8.ImeMode = System.Windows.Forms.ImeMode.AlphaFull;
            this.txtGirl8.Location = new System.Drawing.Point(3, 261);
            this.txtGirl8.MaxLength = 10;
            this.txtGirl8.Name = "txtGirl8";
            this.txtGirl8.Size = new System.Drawing.Size(271, 33);
            this.txtGirl8.TabIndex = 129;
            this.txtGirl8.TextChanged += new System.EventHandler(this.txtGirl8_TextChanged);
            // 
            // lblGirl7
            // 
            this.lblGirl7.AutoSize = true;
            this.lblGirl7.Location = new System.Drawing.Point(280, 236);
            this.lblGirl7.Name = "lblGirl7";
            this.lblGirl7.Size = new System.Drawing.Size(36, 17);
            this.lblGirl7.TabIndex = 193;
            this.lblGirl7.Text = "0/10";
            // 
            // lblGirl6
            // 
            this.lblGirl6.AutoSize = true;
            this.lblGirl6.Location = new System.Drawing.Point(280, 203);
            this.lblGirl6.Name = "lblGirl6";
            this.lblGirl6.Size = new System.Drawing.Size(36, 17);
            this.lblGirl6.TabIndex = 192;
            this.lblGirl6.Text = "0/10";
            // 
            // lblGirl5
            // 
            this.lblGirl5.AutoSize = true;
            this.lblGirl5.Location = new System.Drawing.Point(280, 171);
            this.lblGirl5.Name = "lblGirl5";
            this.lblGirl5.Size = new System.Drawing.Size(36, 17);
            this.lblGirl5.TabIndex = 191;
            this.lblGirl5.Text = "0/10";
            // 
            // txtGirl7
            // 
            this.txtGirl7.Enabled = false;
            this.txtGirl7.Font = new System.Drawing.Font("Meiryo", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGirl7.ImeMode = System.Windows.Forms.ImeMode.AlphaFull;
            this.txtGirl7.Location = new System.Drawing.Point(3, 228);
            this.txtGirl7.MaxLength = 10;
            this.txtGirl7.Name = "txtGirl7";
            this.txtGirl7.Size = new System.Drawing.Size(271, 33);
            this.txtGirl7.TabIndex = 128;
            this.txtGirl7.TextChanged += new System.EventHandler(this.txtGirl7_TextChanged);
            // 
            // txtGirl6
            // 
            this.txtGirl6.Enabled = false;
            this.txtGirl6.Font = new System.Drawing.Font("Meiryo", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGirl6.ImeMode = System.Windows.Forms.ImeMode.AlphaFull;
            this.txtGirl6.Location = new System.Drawing.Point(3, 195);
            this.txtGirl6.MaxLength = 10;
            this.txtGirl6.Name = "txtGirl6";
            this.txtGirl6.Size = new System.Drawing.Size(271, 33);
            this.txtGirl6.TabIndex = 127;
            this.txtGirl6.TextChanged += new System.EventHandler(this.txtGirl6_TextChanged);
            // 
            // txtGirl5
            // 
            this.txtGirl5.Enabled = false;
            this.txtGirl5.Font = new System.Drawing.Font("Meiryo", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGirl5.ImeMode = System.Windows.Forms.ImeMode.AlphaFull;
            this.txtGirl5.Location = new System.Drawing.Point(3, 162);
            this.txtGirl5.MaxLength = 10;
            this.txtGirl5.Name = "txtGirl5";
            this.txtGirl5.Size = new System.Drawing.Size(271, 33);
            this.txtGirl5.TabIndex = 126;
            this.txtGirl5.TextChanged += new System.EventHandler(this.txtGirl5_TextChanged);
            // 
            // lblGirl4
            // 
            this.lblGirl4.AutoSize = true;
            this.lblGirl4.Location = new System.Drawing.Point(280, 137);
            this.lblGirl4.Name = "lblGirl4";
            this.lblGirl4.Size = new System.Drawing.Size(36, 17);
            this.lblGirl4.TabIndex = 186;
            this.lblGirl4.Text = "0/10";
            // 
            // txtGirl4
            // 
            this.txtGirl4.Enabled = false;
            this.txtGirl4.Font = new System.Drawing.Font("Meiryo", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGirl4.ImeMode = System.Windows.Forms.ImeMode.AlphaFull;
            this.txtGirl4.Location = new System.Drawing.Point(3, 129);
            this.txtGirl4.MaxLength = 10;
            this.txtGirl4.Name = "txtGirl4";
            this.txtGirl4.Size = new System.Drawing.Size(271, 33);
            this.txtGirl4.TabIndex = 125;
            this.txtGirl4.TextChanged += new System.EventHandler(this.txtGirl4_TextChanged_1);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 17);
            this.label4.TabIndex = 185;
            this.label4.Text = "Message:";
            // 
            // lblGirl3
            // 
            this.lblGirl3.AutoSize = true;
            this.lblGirl3.Location = new System.Drawing.Point(280, 102);
            this.lblGirl3.Name = "lblGirl3";
            this.lblGirl3.Size = new System.Drawing.Size(36, 17);
            this.lblGirl3.TabIndex = 184;
            this.lblGirl3.Text = "0/10";
            // 
            // lblGirl2
            // 
            this.lblGirl2.AutoSize = true;
            this.lblGirl2.Location = new System.Drawing.Point(280, 69);
            this.lblGirl2.Name = "lblGirl2";
            this.lblGirl2.Size = new System.Drawing.Size(36, 17);
            this.lblGirl2.TabIndex = 183;
            this.lblGirl2.Text = "0/10";
            // 
            // lblGirl1
            // 
            this.lblGirl1.AutoSize = true;
            this.lblGirl1.Location = new System.Drawing.Point(280, 39);
            this.lblGirl1.Name = "lblGirl1";
            this.lblGirl1.Size = new System.Drawing.Size(36, 17);
            this.lblGirl1.TabIndex = 182;
            this.lblGirl1.Text = "0/10";
            // 
            // txtGirl3
            // 
            this.txtGirl3.Enabled = false;
            this.txtGirl3.Font = new System.Drawing.Font("Meiryo", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGirl3.ImeMode = System.Windows.Forms.ImeMode.AlphaFull;
            this.txtGirl3.Location = new System.Drawing.Point(3, 96);
            this.txtGirl3.MaxLength = 10;
            this.txtGirl3.Name = "txtGirl3";
            this.txtGirl3.Size = new System.Drawing.Size(271, 33);
            this.txtGirl3.TabIndex = 124;
            this.txtGirl3.TextChanged += new System.EventHandler(this.txtGirl3_TextChanged);
            // 
            // txtGirl2
            // 
            this.txtGirl2.Enabled = false;
            this.txtGirl2.Font = new System.Drawing.Font("Meiryo", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGirl2.ImeMode = System.Windows.Forms.ImeMode.AlphaFull;
            this.txtGirl2.Location = new System.Drawing.Point(3, 63);
            this.txtGirl2.MaxLength = 10;
            this.txtGirl2.Name = "txtGirl2";
            this.txtGirl2.Size = new System.Drawing.Size(271, 33);
            this.txtGirl2.TabIndex = 123;
            this.txtGirl2.TextChanged += new System.EventHandler(this.txtGirl2_TextChanged);
            // 
            // txtGirl1
            // 
            this.txtGirl1.Enabled = false;
            this.txtGirl1.Font = new System.Drawing.Font("Meiryo", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGirl1.ImeMode = System.Windows.Forms.ImeMode.AlphaFull;
            this.txtGirl1.Location = new System.Drawing.Point(3, 30);
            this.txtGirl1.MaxLength = 10;
            this.txtGirl1.Name = "txtGirl1";
            this.txtGirl1.Size = new System.Drawing.Size(271, 33);
            this.txtGirl1.TabIndex = 122;
            this.txtGirl1.TextChanged += new System.EventHandler(this.txtGirl1_TextChanged);
            // 
            // label2934
            // 
            this.label2934.AutoSize = true;
            this.label2934.Location = new System.Drawing.Point(342, 414);
            this.label2934.Name = "label2934";
            this.label2934.Size = new System.Drawing.Size(36, 17);
            this.label2934.TabIndex = 177;
            this.label2934.Text = "Boy:";
            // 
            // richBoy
            // 
            this.richBoy.Font = new System.Drawing.Font("Meiryo", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richBoy.Location = new System.Drawing.Point(333, 434);
            this.richBoy.Name = "richBoy";
            this.richBoy.ReadOnly = true;
            this.richBoy.Size = new System.Drawing.Size(290, 207);
            this.richBoy.TabIndex = 145;
            this.richBoy.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(0, 414);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 17);
            this.label2.TabIndex = 175;
            this.label2.Text = "Girl:";
            // 
            // richGirl
            // 
            this.richGirl.Font = new System.Drawing.Font("Meiryo", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richGirl.Location = new System.Drawing.Point(3, 434);
            this.richGirl.Name = "richGirl";
            this.richGirl.ReadOnly = true;
            this.richGirl.Size = new System.Drawing.Size(290, 208);
            this.richGirl.TabIndex = 133;
            this.richGirl.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1172, 683);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "DekiruMenuEditor";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Label lblCount4;
        private System.Windows.Forms.TextBox tbString4;
        private System.Windows.Forms.Label lblBlock;
        private System.Windows.Forms.Label lblCount3;
        private System.Windows.Forms.Label lblCount2;
        private System.Windows.Forms.Label lblCount1;
        private System.Windows.Forms.TextBox tbString3;
        private System.Windows.Forms.TextBox tbString2;
        private System.Windows.Forms.TextBox tbString1;
        private System.Windows.Forms.Label lblBoy10;
        private System.Windows.Forms.TextBox txtBoy10;
        private System.Windows.Forms.Label lblBoy9;
        private System.Windows.Forms.TextBox txtBoy9;
        private System.Windows.Forms.Label lblBoy8;
        private System.Windows.Forms.TextBox txtBoy8;
        private System.Windows.Forms.Label lblBoy7;
        private System.Windows.Forms.Label lblBoy6;
        private System.Windows.Forms.Label lblBoy5;
        private System.Windows.Forms.TextBox txtBoy7;
        private System.Windows.Forms.TextBox txtBoy6;
        private System.Windows.Forms.TextBox txtBoy5;
        private System.Windows.Forms.Label lblBoy4;
        private System.Windows.Forms.TextBox txtBoy4;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label lblBoy3;
        private System.Windows.Forms.Label lblBoy2;
        private System.Windows.Forms.Label lblBoy1;
        private System.Windows.Forms.TextBox txtBoy3;
        private System.Windows.Forms.TextBox txtBoy2;
        private System.Windows.Forms.TextBox txtBoy1;
        private System.Windows.Forms.Label lblGirl10;
        private System.Windows.Forms.TextBox txtGirl10;
        private System.Windows.Forms.Label lblGirl9;
        private System.Windows.Forms.TextBox txtGirl9;
        private System.Windows.Forms.Label lblGirl8;
        private System.Windows.Forms.TextBox txtGirl8;
        private System.Windows.Forms.Label lblGirl7;
        private System.Windows.Forms.Label lblGirl6;
        private System.Windows.Forms.Label lblGirl5;
        private System.Windows.Forms.TextBox txtGirl7;
        private System.Windows.Forms.TextBox txtGirl6;
        private System.Windows.Forms.TextBox txtGirl5;
        private System.Windows.Forms.Label lblGirl4;
        private System.Windows.Forms.TextBox txtGirl4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblGirl3;
        private System.Windows.Forms.Label lblGirl2;
        private System.Windows.Forms.Label lblGirl1;
        private System.Windows.Forms.TextBox txtGirl3;
        private System.Windows.Forms.TextBox txtGirl2;
        private System.Windows.Forms.TextBox txtGirl1;
        private System.Windows.Forms.Label label2934;
        private System.Windows.Forms.RichTextBox richBoy;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox richGirl;
        private System.Windows.Forms.RichTextBox tbWhole;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label lblBoy11;
        private System.Windows.Forms.TextBox txtBoy11;
        private System.Windows.Forms.Label lblGirl11;
        private System.Windows.Forms.TextBox txtGirl11;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}

