﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DekiruMenuEdit
{
    class Model
    {
        public uint address { get; set; }   // EXE address not PSX address
        public String str { get; set; }     // the whole string
        public byte type { get; set; }     // 0 for long, 1 for short
        public Boolean changed { get; set; }   // set if data has changed
    }
}
