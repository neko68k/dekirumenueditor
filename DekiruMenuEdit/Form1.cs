﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using ExtensionMethods;

namespace DekiruMenuEdit
{
    public partial class Form1 : Form
    {
        private ArrayList model;
        public static uint startAdr {set; get;}
        public static uint length { set; get; }
        private byte type;
        private int prevSelection;
        private String[] lines;
        string[] girlText;
        string[] boyText;
        private int which;
        private Boolean changed;
        private Boolean girlChanged;
        private Boolean boyChanged;
        string filename;

        public Form1()
        {
            InitializeComponent();
        }

        public void testc(){
            String tstring = "おまけモードに";
            int len = tstring.Length;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            model = new ArrayList();
            startAdr = 0;
            length = 0;
            type = 0;
            treeView1.BeginUpdate();
            treeView1.Nodes.Add("Messages");
            treeView1.EndUpdate();
            tbString1.Enabled = false;
            tbString2.Enabled = false;
            tbString3.Enabled = false;
            tbString4.Enabled = false;
            prevSelection = -1;
            which = -1;
            changed = false;
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Stream exeFile = null;
            Model tempMdl = null;
            uint curAdr = 0;
            byte c = 0;
            uint i = 0;
            char[] delimiterChars = { '\n' };

            // prompt to load MENU.EXE
            openFileDialog1.Filter = "PS-X EXE (*.EXE)|*.EXE|All files (*.*)|*.*";
            openFileDialog1.ShowDialog();
            filename = openFileDialog1.FileName;
            exeFile = openFileDialog1.OpenFile();


            // instead of using manual address
            // use fixed address spaces for particular types of data
            // 47CB4~48A7B = main menu explanations
            // 48A7C&48B7C = assistant manager bio/description (girl, boy. in that order)

            // wont be changing these.
            // 48D5C       = the characters for name selection. 
            // 491DC       = manufacturers but this is padded(not much) unlike in MAIN.EXE
            //Address adrfrm = new Address();
            //adrfrm.ShowDialog();
            startAdr = 0x47CB4;
            length = 0x48A7B - startAdr;
            curAdr = startAdr;
            exeFile.Seek(startAdr, SeekOrigin.Begin);

            // buffer data
            //exeFile.Read(buf, 0, (int)length);
            BinaryReader rdr = new BinaryReader(exeFile, Encoding.GetEncoding("shift-jis"));
            while (curAdr < startAdr + length)
            {
                
                tempMdl = new Model();
                // load the strings here

                tempMdl.str = rdr.ReadNullString();
                tempMdl.address = curAdr;
                int len = tempMdl.str.Length;

                // now step through each byte until we hit something
                // that is non null, check the total length in bytes
                
                while((c = rdr.ReadByte()) == 0x00)
                {}
                  

                exeFile.Position -= 1;

                uint size = (uint)exeFile.Position - curAdr;

                tempMdl.type = 0;

                treeView1.BeginUpdate();
                treeView1.Nodes[0].Nodes.Add(String.Format("{0}", i));
                treeView1.EndUpdate();
                // bump loop 
                curAdr += size;
                model.Add(tempMdl);
                
                i++;
            }
            treeView1.ExpandAll();



            // assistant manager portion 48A7C 48B7C 
            startAdr = 0x48A7C;
            exeFile.Seek(startAdr, SeekOrigin.Begin);
            richGirl.Text = rdr.ReadNullString();
            girlText = richGirl.Text.Split(delimiterChars);

            char[] trim = { '\n' };

            girlText[0] = girlText[0].TrimEnd(trim);
            girlText[1] = girlText[1].TrimEnd(trim);
            girlText[2] = girlText[2].Substring(1).TrimEnd(trim);
            girlText[3] = girlText[3].TrimEnd(trim);
            girlText[4] = girlText[4].Substring(1).TrimEnd(trim);
            girlText[5] = girlText[5].TrimEnd(trim);
            girlText[6] = girlText[6].Substring(1).TrimEnd(trim);
            girlText[7] = girlText[7].TrimEnd(trim);
            girlText[8] = girlText[8].Substring(1).TrimEnd(trim);
            girlText[9] = girlText[9].TrimEnd(trim);
            girlText[10] = girlText[10].Substring(1).TrimEnd(trim);
            girlText[11] = girlText[11].TrimEnd(trim);
            girlText[12] = girlText[12].Substring(1).TrimEnd(trim);
            girlText[13] = girlText[13].TrimEnd(trim);
            girlText[14] = girlText[14].Substring(1).TrimEnd(trim);
            girlText[15] = girlText[15].TrimEnd(trim);
            girlText[16] = girlText[16].Substring(1).TrimEnd(trim);
            girlText[17] = girlText[17].TrimEnd(trim);
            girlText[18] = girlText[18].Substring(1).TrimEnd(trim);
            girlText[19] = girlText[19].TrimEnd(trim);
            girlText[20] = girlText[20].Substring(1).TrimEnd(trim);

            txtGirl1.Text = girlText[0];
            txtGirl2.Text = girlText[2];
            txtGirl3.Text = girlText[4];
            txtGirl4.Text = girlText[6];
            txtGirl5.Text = girlText[8];
            txtGirl6.Text = girlText[10];
            txtGirl7.Text = girlText[12];
            txtGirl8.Text = girlText[14];
            txtGirl9.Text = girlText[16];
            txtGirl10.Text = girlText[18];
            txtGirl11.Text = girlText[20];

            /*StringBuilder sb = new StringBuilder();
            
            for(i = 0;i<21;i+=2){

                String tmp = girlText[i];
                sb.AppendLine(tmp);
            }

            richGirl.Text = sb.ToString();*/

            startAdr = 0x48B7C;
            exeFile.Seek(startAdr, SeekOrigin.Begin);
            richBoy.Text = rdr.ReadNullString();
            boyText = richBoy.Text.Split(delimiterChars);


            boyText[2] = boyText[2].Substring(1);
            boyText[4] = boyText[4].Substring(1);
            boyText[6] = boyText[6].Substring(1);
            boyText[8] = boyText[8].Substring(1);
            boyText[10] = boyText[10].Substring(1);
            boyText[12] = boyText[12].Substring(1);
            boyText[14] = boyText[14].Substring(1);
            boyText[16] = boyText[16].Substring(1);
            boyText[18] = boyText[18].Substring(1);
            boyText[20] = boyText[20].Substring(1);

            boyText[0] = boyText[0].TrimEnd(trim);
            boyText[1] = boyText[1].TrimEnd(trim);
            boyText[3] = boyText[3].TrimEnd(trim);
            boyText[7] = boyText[7].TrimEnd(trim);
            boyText[9] = boyText[9].TrimEnd(trim);
            boyText[11] = boyText[11].TrimEnd(trim);
            boyText[13] = boyText[13].TrimEnd(trim);
            boyText[15] = boyText[15].TrimEnd(trim);
            boyText[17] = boyText[17].TrimEnd(trim);
            boyText[19] = boyText[19].TrimEnd(trim);

            txtBoy1.Text = boyText[0];
            txtBoy2.Text = boyText[2];
            txtBoy3.Text = boyText[4];
            txtBoy4.Text = boyText[6];
            txtBoy5.Text = boyText[8];
            txtBoy6.Text = boyText[10];
            txtBoy7.Text = boyText[12];
            txtBoy8.Text = boyText[14];
            txtBoy9.Text = boyText[16];
            txtBoy10.Text = boyText[18];
            txtBoy11.Text = boyText[20];

            txtGirl1.Enabled = true;
            txtGirl2.Enabled = true;
            txtGirl3.Enabled = true;
            txtGirl4.Enabled = true;
            txtGirl5.Enabled = true;
            txtGirl6.Enabled = true;
            txtGirl7.Enabled = true;
            txtGirl8.Enabled = true;
            txtGirl9.Enabled = true;
            txtGirl10.Enabled = true;
            txtGirl11.Enabled = true;

            txtBoy1.Enabled = true;
            txtBoy2.Enabled = true;
            txtBoy3.Enabled = true;
            txtBoy4.Enabled = true;
            txtBoy5.Enabled = true;
            txtBoy6.Enabled = true;
            txtBoy7.Enabled = true;
            txtBoy8.Enabled = true;
            txtBoy9.Enabled = true;
            txtBoy10.Enabled = true;
            txtBoy11.Enabled = true;

            /*sb.Clear();
            for (i = 0; i < 21; i += 2)
            {


                String tmp = girlText[i];
                sb.AppendLine(tmp);

            }

            richBoy.Text = sb.ToString();*/

            rdr.Close();
            exeFile.Close();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // if "changed" flag prompt to save before quitting
           
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            testc();
        }

        private void updateModel(){
            if (which != -1)
            {
                if (lines.Length == 4)
                {
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < 4; i++)
                    {
                        if (i != 3)
                        {
                            int numSpaces = 16 - (lines[i].Length * 2);
                            sb.Append(lines[i]);
                            for (int j = 0; j < numSpaces; j++)
                            {
                                sb.Append(" ");
                            }

                            sb.Append("\n");
                        }
                        else
                            sb.Append(lines[i]);
                    }
                    if (String.Compare(((Model)model[which]).str, sb.ToString()) != 0)
                    {
                        changed = true;
                        if (treeView1.Nodes[0].Nodes[which].Text.IndexOf("*") == -1)
                            treeView1.Nodes[0].Nodes[which].Text += "*";
                        ((Model)model[which]).str = sb.ToString();
                        ((Model)model[which]).changed = true;
                    }
                    sb.Clear();
                }
                else
                {
                    if (treeView1.Nodes[0].Nodes[which].Text.IndexOf("*") == -1)
                        treeView1.Nodes[0].Nodes[which].Text += "*";
                    ((Model)model[which]).str = lines[0];
                    ((Model)model[which]).changed = true;
                }
            }
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {   
            char[] delimiterChars = { '\n'};

            if (e.Node.Text != "Messages")
            {
                updateModel();

                char[] trim = { '*' };
                which = Int32.Parse(e.Node.Text.TrimEnd(trim));

                lines = ((Model)model[which]).str.Split(delimiterChars);
                lblBlock.Text = String.Format("Message: {0}", which);
                if (lines.Length == 1)
                {

                    tbString1.Text = lines[0];
                    tbString2.Text = "";
                    tbString3.Text = "";
                    tbString1.Enabled = true;
                    tbString2.Enabled = false;
                    tbString3.Enabled = false;
                    tbString4.Enabled = false;
                    if (((Model)model[which]).str.Length > 8)
                    {
                        tbString1.MaxLength = 16;
                        lblCount1.Text = String.Format("{0}/16", tbString1.Text.Length);
                        type = 1;
                    }
                    else
                    {
                        tbString1.MaxLength = 8;
                        lblCount1.Text = String.Format("{0}/8", tbString1.Text.Length);
                        type = 0;
                    }
                    lblCount2.Text = "0/0";
                    lblCount3.Text = "0/0";
                    lblCount4.Text = "0/0";
                }
                else if (lines.Length == 4)
                {
                    type = 0;
                    tbString1.MaxLength = 8;
                    tbString1.Text = lines[0].Substring(0, 16 - lines[0].Length);
                    tbString2.Text = lines[1].Substring(0, 16 - lines[1].Length);
                    tbString3.Text = lines[2].Substring(0, 16 - lines[2].Length);
                    if (lines[3].Length != 0)
                    {
                        tbString4.Text = lines[3];
                        lines[3] = tbString4.Text;
                    }
                    else
                    {
                        tbString4.Text = "";
                    }

                    lines[0] = tbString1.Text;
                    lines[1] = tbString2.Text;
                    lines[2] = tbString3.Text;

                    tbString1.Enabled = true;
                    tbString2.Enabled = true;
                    tbString3.Enabled = true;
                    tbString4.Enabled = true;
                    lblCount1.Text = String.Format("{0}/8", tbString1.Text.Length);
                    lblCount2.Text = String.Format("{0}/8", tbString2.Text.Length);
                    lblCount3.Text = String.Format("{0}/8", tbString3.Text.Length);
                    lblCount4.Text = String.Format("{0}/8", tbString4.Text.Length);
                }
            }
        }

        private void tbString1_TextChanged(object sender, EventArgs e)
        {
            if (type == 0)
            {
                lblCount1.Text = String.Format("{0}/8", tbString1.Text.Length);
            }
            else
            {
                lblCount1.Text = String.Format("{0}/16", tbString1.Text.Length);
            }
            lines[0] = tbString1.Text;
            
            tbWhole.Text = String.Join("", lines);
        }

        private void tbString2_TextChanged_1(object sender, EventArgs e)
        {
            lblCount2.Text = String.Format("{0}/8", tbString2.Text.Length);
            if (lines.Length > 1)
            {
                lines[1] = tbString2.Text;
            }
            
            
            tbWhole.Text = String.Join("", lines);
        }

        private void tbString3_TextChanged_1(object sender, EventArgs e)
        {
            lblCount3.Text = String.Format("{0}/8", tbString3.Text.Length);
            if (lines.Length > 1)
            {
                lines[2] = tbString3.Text;
            }
            
            tbWhole.Text = String.Join("", lines);
        }

        private void tbString4_TextChanged_1(object sender, EventArgs e)
        {
            lblCount4.Text = String.Format("{0}/8", tbString4.Text.Length);
            if (lines.Length > 1)
            {
                lines[3] = tbString4.Text;
            }
            
            tbWhole.Text = String.Join("", lines);
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            save(filename);
        }

        private void save(String fn)
        {
            // get the data out of the model and write it back to where it came from!
            updateModel();  // update to make sure the selected one is GTG

            // step through model and if(changed) then write back to stored address
            // strings probably need to be converted back to SJIS
            Stream outStream = File.Open(fn, FileMode.OpenOrCreate);
            BinaryWriter bs = new BinaryWriter(outStream, Encoding.GetEncoding("shift-jis"));
            for (int i = 0; i < model.Count; i++)
            {
                Model mod = (Model)model[i];
                if (mod.changed)
                {
                    char[] trim = { '*' };
                    treeView1.Nodes[0].Nodes[i].Text = treeView1.Nodes[0].Nodes[i].Text.TrimEnd(trim);
                    ((Model)model[i]).changed = false;
                    int z = Encoding.GetEncoding("shift-jis").GetBytes(mod.str).Length;   
                    outStream.Seek(mod.address, SeekOrigin.Begin);
                    // write the string
                    bs.WriteNullString(mod.str);

                    // pad out null stuff
                    for (int j = 0; j < 0x48 - z; j++)
                    {
                        bs.Write((byte)0);
                    }
                }
            }

            /*
            startAdr = 0x47CB4;
            length = 0x48A7B - startAdr;
            // assistant manager portion 48A7C 48B7C 
            startAdr = 0x48A7C;
             */
            // girl page
            StringBuilder sb = new StringBuilder();

            if (girlChanged)
            {
                outStream.Seek(0x48A7C, SeekOrigin.Begin);

                // do the first one now since it doesn't get a tab
                sb.Append(girlText[0]);
                sb.Append("\n");

                for (int i = 1; i < girlText.Length; i++)
                {
                    if (i % 2 == 0)
                    {
                        sb.Append("\t");
                    }
                    
                    sb.Append(girlText[i]);
                    sb.Append("\n");
                    
                }

                bs.WriteNullString(sb.ToString());
                int z = Encoding.GetEncoding("shift-jis").GetBytes(sb.ToString()).Length;   
                for (int j = 0; j < 0x100 - z; j++)
                {
                    bs.Write((byte)0);
                }


                sb.Clear();
            } 
            if(boyChanged){
                // boy page
                outStream.Seek(0x48B7C, SeekOrigin.Begin);

                sb.Append(boyText[0]);
                sb.Append("\n");

                for (int i = 1; i < boyText.Length; i++)
                {
                    if (i % 2 == 0)
                    {
                        sb.Append("\t");
                    }
                    sb.Append(boyText[i]);
                    sb.Append("\n");
                }
                bs.WriteNullString(sb.ToString());
                int z = Encoding.GetEncoding("shift-jis").GetBytes(sb.ToString()).Length;
                for (int j = 0; j < 0x100 - z; j++)
                {
                    bs.Write((byte)0);
                }
            }

            bs.Close();
        }

        private void txtGirl2_TextChanged(object sender, EventArgs e)
        {
            lblGirl2.Text = String.Format("{0}/10", txtGirl2.Text.Length);
            girlText[2] = txtGirl2.Text;
            updateGirl();
        }

        private void txtGirl3_TextChanged(object sender, EventArgs e)
        {
            lblGirl3.Text = String.Format("{0}/10", txtGirl3.Text.Length);
            girlText[4] = txtGirl3.Text;
            updateGirl();
        }

        

        private void txtGirl5_TextChanged(object sender, EventArgs e)
        {
            lblGirl5.Text = String.Format("{0}/10", txtGirl5.Text.Length);
            girlText[8] = txtGirl5.Text;
            updateGirl();
        }

        private void txtGirl6_TextChanged(object sender, EventArgs e)
        {
            lblGirl6.Text = String.Format("{0}/10", txtGirl6.Text.Length);
            girlText[10] = txtGirl6.Text;
            updateGirl();
        }

        private void txtGirl7_TextChanged(object sender, EventArgs e)
        {
            lblGirl7.Text = String.Format("{0}/10", txtGirl7.Text.Length);
            girlText[12] = txtGirl7.Text;
            updateGirl();
        }

        private void txtGirl8_TextChanged(object sender, EventArgs e)
        {
            lblGirl8.Text = String.Format("{0}/10", txtGirl8.Text.Length);
            girlText[14] = txtGirl8.Text;
            updateGirl();
        }

        private void txtGirl9_TextChanged(object sender, EventArgs e)
        {
            lblGirl9.Text = String.Format("{0}/10", txtGirl9.Text.Length);
            girlText[16] = txtGirl9.Text;
            updateGirl();
        }

        private void txtGirl10_TextChanged(object sender, EventArgs e)
        {
            lblGirl10.Text = String.Format("{0}/10", txtGirl10.Text.Length);
            girlText[18] = txtGirl10.Text;
            updateGirl();
        }

        private void txtGirl11_TextChanged(object sender, EventArgs e)
        {
            lblGirl11.Text = String.Format("{0}/10", txtGirl11.Text.Length);
            girlText[20] = txtGirl11.Text;
            updateGirl();
        }

        private void updateGirl()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 21; i += 2)
            {
                String tmp = girlText[i];
                sb.AppendLine(tmp);
            }

            richGirl.Text = sb.ToString();
            girlChanged = true;
        }

        private void updateBoy()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 21; i += 2)
            {
                String tmp = boyText[i];
                sb.AppendLine(tmp);
            }

            richBoy.Text = sb.ToString();
            boyChanged = true;
        }

        private void txtBoy1_TextChanged(object sender, EventArgs e)
        {
            lblBoy1.Text = String.Format("{0}/10", txtBoy1.Text.Length);
            boyText[0] = txtBoy1.Text;
            updateBoy();
        }

        private void txtBoy2_TextChanged(object sender, EventArgs e)
        {
            lblBoy2.Text = String.Format("{0}/10", txtBoy2.Text.Length);
            boyText[2] = txtBoy2.Text;
            updateBoy();
        }

        private void txtBoy3_TextChanged(object sender, EventArgs e)
        {
            lblBoy3.Text = String.Format("{0}/10", txtBoy3.Text.Length);
            boyText[4] = txtBoy3.Text;
            updateBoy();
        }



        private void txtBoy5_TextChanged(object sender, EventArgs e)
        {
            lblBoy5.Text = String.Format("{0}/10", txtBoy5.Text.Length);
            boyText[8] = txtBoy5.Text;
            updateBoy();
        }

        private void txtBoy6_TextChanged(object sender, EventArgs e)
        {
            lblBoy6.Text = String.Format("{0}/10", txtBoy6.Text.Length);
            boyText[10] = txtBoy6.Text;
            updateBoy();
        }

        private void txtBoy7_TextChanged(object sender, EventArgs e)
        {
            lblBoy7.Text = String.Format("{0}/10", txtBoy7.Text.Length);
            boyText[12] = txtBoy7.Text;
            updateBoy();
        }

        private void txtBoy8_TextChanged(object sender, EventArgs e)
        {
            lblBoy8.Text = String.Format("{0}/10", txtBoy8.Text.Length);
            boyText[14] = txtBoy8.Text;
            updateBoy();
        }

        private void txtBoy9_TextChanged(object sender, EventArgs e)
        {
            lblBoy9.Text = String.Format("{0}/10", txtBoy9.Text.Length);
            boyText[16] = txtBoy9.Text;
            updateBoy();
        }

        private void txtBoy10_TextChanged(object sender, EventArgs e)
        {
            lblBoy10.Text = String.Format("{0}/10", txtBoy10.Text.Length);
            boyText[18] = txtBoy10.Text;
            updateBoy();
        }

        private void txtBoy11_TextChanged(object sender, EventArgs e)
        {
            lblBoy11.Text = String.Format("{0}/10", txtBoy11.Text.Length);
            boyText[20] = txtBoy11.Text;
            updateBoy();
        }

        private void txtGirl1_TextChanged(object sender, EventArgs e)
        {
            lblGirl1.Text = String.Format("{0}/10", txtGirl1.Text.Length);
            girlText[0] = txtGirl1.Text;
            updateGirl();
        }

        private void txtGirl4_TextChanged_1(object sender, EventArgs e)
        {
            lblGirl4.Text = String.Format("{0}/10", txtGirl4.Text.Length);
            girlText[6] = txtGirl4.Text;
            updateGirl();
        }

        private void txtBoy4_TextChanged(object sender, EventArgs e)
        {
            lblBoy4.Text = String.Format("{0}/10", txtBoy4.Text.Length);
            boyText[6] = txtBoy4.Text;
            updateBoy();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "PS-X EXE (*.EXE)|*.EXE|All files (*.*)|*.*";
            
            //if(saveFileDialog1.ShowDialog() == DialogResult.OK)
            //    save(openFileDialog1.FileName);
        }

    }
}

