﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DekiruMenuEdit
{
    public partial class Address : Form
    {
        public Address()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            uint tmpadr = UInt32.Parse(textBox1.Text, System.Globalization.NumberStyles.HexNumber);
            Form1.startAdr = tmpadr;
            Form1.length = UInt32.Parse(textBox2.Text, System.Globalization.NumberStyles.HexNumber)-tmpadr;
            this.Close();
        }
    }
}
