﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtensionMethods
{
    public static class MyExtensions
    {
        public static string ReadNullString(this System.IO.BinaryReader stream)
        {
            string str = "";
            char ch;
            while ((int)(ch = stream.ReadChar()) != 0)
                str = str + ch;
            return str;
        }

        public static void WriteNullString(this System.IO.BinaryWriter stream, String str)
        {
            byte[] bytes = Encoding.GetEncoding("shift-jis").GetBytes(str);
            for (int i = 0; i < bytes.Length; i++)
            {
                    stream.Write(bytes[i]);
            }
            /*for (int i = 0; i < (0x48-bytes.Length); i++)
            {
                stream.Write((byte)0);
            }*/
        }
    }
}
